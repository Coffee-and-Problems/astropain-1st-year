recursive integer function fun2(a,k) result(w)
implicit none
integer a,k

if (a.eq.2) then
w=0
else
  if (mod(a,2).eq.0) then
  w=1
  else
  w=0
    if ((mod(a,k).eq.0).and.(k.le.(a/k))) then
    w=1
    else
      if (k.le.(a/k)) then
      w=fun2(a,k+2)
      endif
    endif
  endif
endif

end function
