program punctg
implicit none
integer a,b,aa,bb,fun1,fun2
integer :: ninp=10,nres=11
real artime(2),t0,t1,t2,t10,t21

open(unit=ninp,file='input')
open(unit=nres,file='output',status='replace')
read(ninp,*) a
aa=a

if (a.eq.1) then
 bb=2
 b=2
else
 call etime(artime,t0)
 bb=fun1(aa)
 call etime(artime,t1)
 b=fun2(a)
 call etime(artime,t2)
 t10=t1-t0
 t21=t2-t1
endif


write(nres,*) "      1) fun1=",bb,"        2) fun2=",b
write(nres,*) "нерекурсивная ",t10," рекурсивная",t21

end
