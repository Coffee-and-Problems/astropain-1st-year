program puncta
implicit none
integer a,b,aa,bb,c,cc,fun1,fun2
integer :: ninp=10,nres=11
real artime(2),t0,t1,t2,t10,t21

open(unit=ninp,file='input')
open(unit=nres,file='output',status='replace')

read(ninp,*) a,b
aa=a
bb=b

call etime(artime,t0)
cc=fun1(aa,bb)
call etime(artime,t1)
c=fun2(a,b)
call etime(artime,t2)
t10=t1-t0
t21=t2-t1

write(nres,*) "      1)   a/b=",cc,"       2)   a/b=",c
write(nres,*) "нерекурсивная ",t10," рекурсивная",t21
end
