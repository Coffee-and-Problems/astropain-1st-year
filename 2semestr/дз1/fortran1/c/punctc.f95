program punctc
implicit none
integer(16) a,aa,b,bb,fun1,fun2
integer :: ninp=10,nres=11
real artime(2),t0,t1,t2,t10,t21

open(unit=ninp,file='input')
open(unit=nres,file='output',status='replace')
read(ninp,*) a
aa=a

call etime(artime,t0)
bb=fun1(aa)
call etime(artime,t1)
b=fun2(a)
call etime(artime,t2)
t10=t1-t0
t21=t2-t1

write(nres,*) "          1)",bb,"              2)",b
write(nres,*) "нерекурсивная ",t10," рекурсивная",t21

end
