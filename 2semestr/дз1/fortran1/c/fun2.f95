recursive integer(16) function fun2(a) result(w)
implicit none
integer(16) a

if (a.eq.0) then
 w=0
else
 w=10*fun2(a/2)+mod(a,2)
endif

end function
