recursive integer(16) function fun2(a,b) result(w)
implicit none
integer(16) a,b

if (b.eq.1) then
w=a
else
w=a*fun2(a,b-1)
endif

end function
