program tsthanoi
implicit none
integer n
integer :: ninp=10, nres=11
real artime(2), t0, t1, t10

open(unit=ninp,file='input')
open(unit=nres,file='output',status='replace')

read (ninp,*) n
write (nres,*) ' n=',n
call etime(artime,t0)
call hanoi(n,1,3)
call etime(artime,t1)
t10=t1-t0
write(nres,*) 't10=',t10
end
