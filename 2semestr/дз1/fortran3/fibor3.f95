recursive integer function fibor3(n,f) result(w)
implicit none
integer n
integer f(100)
if ((n.eq.1).or.(n.eq.2)) then
f(n)=1
w=1
endif
if (f(n).ne.0) then
w=f(n)
else
f(n)=fibor3(n-1,f)+fibor3(n-2,f)
w=f(n)
endif
end