recursive integer function fibor1(n) result(w)
implicit none
integer n
if (n<3) then; w=1
else
w=fibor1(n-1)+fibor1(n-2)
endif
end