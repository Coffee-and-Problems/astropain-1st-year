Program ts
use my_prec
use fun
implicit none


real(mp) t0, tn, pi2, ht, x, r0, r1, t
integer n, ninp, nres, i

data ninp /5/, nres /6/

open(unit=ninp, file='input')
open(unit=nres, file='result',status='replace')

read(ninp,100) t0, tn
read(ninp,101) n

write(nres,*)'# t0=',t0,' tn=',tn,' n=',n
pi2=2*atan(1.0)
ht=(tn-t0)/n

write(nres,1100)
do i=0,n
 t=t0+i*ht
 x=pi2-exp(-t)
 r0=tg0(x)
 r1=tg1(x)
 write(nres,1001) i, t, r0, r1
enddo
close(nres)

100 format(e15.7)
101 format(i15)
1100 format(1x,' #',2x,'i',12x,'t',14x,'tg0',18x, 'tg1')
1001 format(1x,i5,2x,e15.7,2x,e25.16,e25.16)

contains

function tg0(x)
real(mp) tg0, tg, tg2, x
tg=tan(x)
tg2=tg*tg
tg0=sqrt(tg2+1.23_mp/cos(x))-tg
return
end function


function tg1(x)
use my_prec
implicit none
real(mp) sin2, tg1, x
tg1=1.23_mp/((sqrt(sin(x)*sin(x)+1.23_mp*cos(x))+sin(x)))
return
end function

end program