function tg0(x)
use my_prec
implicit none
real(mp) tg0, tg, tg2, x
tg=tan(x)
tg2=tg*tg
tg0=sqrt(tg2+1.23_mp/cos(x))-tg
return
end function