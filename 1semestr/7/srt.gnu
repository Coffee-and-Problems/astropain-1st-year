set terminal x11
set output

plot 'result' using 2:3 with linespoints ps 5,\
	''    using 2:4 with linespoints ps 3
pause 10 "p(n)/b and p1(n)/b1"
set terminal postscript eps enhanced
set output "result.eps"
replot
reset