Program ts
use my_prec
implicit none
real(mp) a,b, pi2, ht, x, t, r0, res, tab
integer i, n, ninp, nres

data ninp /5/, nres /6/
open(unit=ninp, file='input')
open(unit=nres, file='result',status='replace')

read(ninp,100) a, b
read(ninp,101) n

write(nres,*) a, b, n
pi2=2*atan(1.0)
ht=(b-a)/n

write(*,*) 1_mp/2.2_mp
write(nres,1100)
do i=0,n
 t=a+i*ht
 r0=tab(t)
 call subfun(t, res)
 write(nres,1001) i, t, r0, res
enddo
close(nres)

100 format(e15.7)
101 format(i15)
1100 format(1x,' #',2x,'i',12x,'t',14x,'tg0',14x, 'tg1')
1001 format(1x,i5,2x,e15.7,e15.7,2x,e15.7,e15.7)

end program