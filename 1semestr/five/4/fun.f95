function cube(eps,D)
use my_prec
implicit none
real(mp) eps,D,x0,x1,cube

x0=1

x1=(2*x0 + D/(x0*x0) )/ 3

do while(abs(x0-x1).gt.eps)
    x0=x1
    x1=(2_mp*x0+D/(x0*x0))/3_mp

enddo
cube=x1
end function
