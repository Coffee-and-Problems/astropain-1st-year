subroutine sub_hz4(eps,D,x1)
use my_prec
implicit none
real(mp) x0,x1,eps,D

x0=1_mp
x1=(2 *x0 + D / (x0 * x0)) / 3

do while(abs(x0-x1).gt.eps)
x0=x1
x1=(2  * x0 + D / (x0 * x0)) / 3

enddo
end subroutine
