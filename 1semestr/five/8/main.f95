program eight
use my_prec
implicit none
real(mp) x,fun,funpr
integer(mp) i

x=0
write (*,*) 1.0_mp/3.2_mp

do i=1,10
  x=i*0.1_mp
  write (*,*) "i=",i,"x=",x
  write (*,*) fun(x), funpr(x)
  write (*,*) "______________________________________________"
enddo

end program eight
