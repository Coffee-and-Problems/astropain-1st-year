program dz5zad5
use my_prec
implicit none
real(mp) eps,fun,res
eps=1
write (*,*) fun(eps)

call subfun(eps,res)
write (*,*) res, epsilon(0.0_mp), tiny(0.0_mp), huge(0.0_mp)
end