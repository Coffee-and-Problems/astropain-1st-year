function fun(eps)
use my_prec
implicit none
real(mp) eps,fun
do while ((1+eps)>1)
eps=eps/2
enddo
fun=eps*2
return
end function