subroutine subfun(eps,res)
use my_prec
implicit none
real(mp) eps,res
do while ((1+eps)>1)
eps=eps/2
enddo
res=eps*2
end subroutine