function fun(x,y)
use my_prec
implicit none
integer(mp) x,y,fun
do while (y.ne.0)
fun=mod(x,y)
x=y
y=fun
enddo
fun=x
return
end function