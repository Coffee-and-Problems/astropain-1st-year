function funpr(x)
use my_prec
implicit none
real(mp) x,funpr
funpr=1/(1.6_mp+sqrt(2.56_mp-x**21))
return
end function