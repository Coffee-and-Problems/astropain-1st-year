program dz5zad1
use my_prec
implicit none
real(mp) a,b,x
integer(mp) fun,res

read (*,*) a,b
write (*,*) a,b

 select case (fun(a,b,x))
  case (0); write (*,*) "нет корней"
  case (1); write (*,*) "корней бесконечно много"
  case (2); write (*,*) x
 endselect

call subfun(a,b,x,res)

 select case (res)
  case (0); write (*,*) "нет корней"
  case (1); write (*,*) "корней бесконечно много"
  case (2); write (*,*) x
 endselect

end