Program o
implicit none
real(4) :: x,y
integer :: ix, iy
integer, dimension(-1:1,-1:1) :: tab
data tab/-3,-1,4,-2,0,2,-4,1,3/

read(*,*) x,y

Ix=0
Iy=0

if(x/=0) then
  Ix=int(x/abs(x))
endif

if(y/=0) then 
  Iy=int(y/abs(y))
endif

write(*,*) tab(Iy,Ix)
end program
