set terminal qt
set output
plot 'result' using 2:3 with lp lw 3 pt 7 ps 1
pause 2 "result"

set terminal postscript eps enhanced
set output "result.eps"

replot
reset

plot 'result' using 2:4 with lp lw 3 pt 7 ps 4
pause 3 "result"

set terminal postscript eps enhanced
set output "result1.eps"

replot
reset
