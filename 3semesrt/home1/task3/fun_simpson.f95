function simpson(f,a,b,n)
use my_prec
implicit none

integer n, i
real(mp) a, b, simpson, f
real(mp) h, x, s1,s2
h=(b-a)/n
s1=0._mp;s2=0._mp
do i=2,n
    x=a+(i-1)*h
    if(mod(i,2)==0) then
    s1=s1+f(x)
    else
    s2=s2+f(x)
    endif
enddo
simpson=(f(a)+f(b)+4_mp*s1+2_mp*s2)*h/3_mp
end
