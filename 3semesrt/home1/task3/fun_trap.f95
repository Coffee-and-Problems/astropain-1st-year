function trap(f,a,b,n)
use my_prec
implicit none

integer n, i
real(mp) a, b, trap, f
real(mp) h, x, s
h=(b-a)/n
s=(f(a)+f(b))/2_mp
do i=2,n
    x=a+(i-1_mp)*h
    s=s+f(x)
enddo
trap=s*h
end
