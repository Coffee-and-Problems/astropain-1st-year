program test
use myfun
use quadramod

implicit none
!interface

!function f(x);real x; end function f
!function trap(f,a,b,n);integer n;real a,b;real, external :: f; end function trap
!function rectan(f,a,b,n);integer n;real a,b;real,external :: f; end function rectan
!function simpson(f,a,b,n);integer n;real a,b;real,external :: f; end function simpson
!function quadra(fqua,f,a,b,n);integer n;real a,b;real,external :: f,fqua; end function quadra
!end interface

integer n /10/ !колл-во участков дробления
real(mp) a /1.0_mp/, b /10.0_mp/


write(*,*)n,quadra(trap,f,a,b,n)
write(*,*)n, quadra(rectan,f,a,b,n)
write(*,*)n,quadra( simpson,f,a,b,n)


end program