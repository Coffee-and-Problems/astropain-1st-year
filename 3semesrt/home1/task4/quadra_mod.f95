module quadramod
use my_prec
use myfun
contains

function quadra(fqua,f,a,b,n)

implicit none
external fqua, f
real(mp) quadra,fqua,a,b,f
integer n
quadra=fqua(f,a,b,n)
end function

function rectan(f,a,b,n)

implicit none
external fqua, f
integer n, i
real(mp) a, b, rectan,f
real(mp) h, x, s
h=(b-a)/n
s=f(a+h/2_mp)
do i=2,n
    x=a+h*(i-0.5_mp)
    s=s+f(x)
enddo
rectan=s*h
end

function simpson(f,a,b,n)

implicit none
external fqua, f
integer n, i
real(mp) a, b, simpson,f
real(mp) h, x, s1,s2
h=(b-a)/n
s1=0._mp;s2=0._mp
do i=2,n
    x=a+(i-1)*h
    if(mod(i,2)==0) then
    s1=s1+f(x)
    else
    s2=s2+f(x)
    endif
enddo
simpson=(f(a)+f(b)+4_mp*s1+2_mp*s2)*h/3_mp
end

function trap(f,a,b,n)

implicit none
external fqua, f
integer n, i
real(mp) a, b, trap,f
real(mp) h, x, s
h=(b-a)/n
s=(f(a)+f(b))/2_mp
do i=2,n
    x=a+(i-1_mp)*h
    s=s+f(x)
enddo
trap=s*h
end


end module quadramod
