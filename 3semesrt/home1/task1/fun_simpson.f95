function simpson(f,a,b,n)
implicit none
integer n, i
real a, b, simpson, f
real h, x, s1,s2
h=(b-a)/n
s1=0;s2=0
do i=2,n
    x=a+(i-1)*h
    if(mod(i,2)==0) then
    s1=s1+f(x)
    else
    s2=s2+f(x)
    endif
enddo
simpson=(f(a)+f(b)+4*s1+2*s2)*h/3.0
end