function rectan(f,a,b,n)
use my_prec
implicit none

integer n, i
real(mp) a, b, rectan, f
real(mp) h, x, s
h=(b-a)/n
s=f(a+h/2_mp)
do i=2,n
    x=a+h*(i-0.5_mp)
    s=s+f(x)
enddo
rectan=s*h
end